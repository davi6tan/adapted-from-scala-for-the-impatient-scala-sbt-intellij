*Work exercises adapted from :*

# Scala for the Impatient

### Table of Contents

*The [AL][1-3] refer to Martin Odersky's Scala levels*

- 01 The Basics (A1)

- 02 Control Structures and Functions (A1)

- 03 Arrays (A1)

- 04 Maps and Tuples (A1)

- 05 Classes (A1)

- 06 Objects (A1)

- 07 Packages and Imports (A1)

- 08 Inheritance (A1)

- 09 Files and Regular Expressions (A1)

- 10 Traits (L1)

- 11 Operators (L1)

- 12 Higher-Order Functions (L1)

- 13 Collections (A2)

- 14 Pattern Matching and Case Classes (A2)

- 15 Annotations (A2)

- 16 XML Processing (A2)

- 17 Type Parameters (L2)

- 18  Advanced Types  (L2)

- 19 Parsing and Domain-Specific Languages (A3)

- 20 Actors (A3)

- 21 Implicits (L3)

- 22 Delimited Continuations (L3)














*[www.horstmann.com ](http://www.horstmann.com/scala/index.html)*