
class Counter(private var value: Int = 0) {
	def increment() { if (value < Int.MaxValue) value += 1 }
	def current = value
}
println(Int.MaxValue)//2147483647

val c = new Counter(Int.MaxValue - 1)

println(c.current)
c.increment()
println(c.current)
c.increment()
println(c.current)
/*
Davids-MacBook-Pro:Chapter05Classes davidtan$ scala 01.scala
2147483646
2147483647
2147483647
 */