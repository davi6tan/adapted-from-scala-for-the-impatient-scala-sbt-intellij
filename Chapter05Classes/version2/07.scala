

class Person(name:String){
  val first:String = name.split("\\s+")(0)
  val last:String = name.split("\\s+")(1)
  override def toString = "Person(First name is :"+first+" last name is : "+last+")"
  //override def toString = "Person(%s,%s)".format(first,last)
}

val person = new Person("John Smoith")

println(person)

/*
Davids-MacBook-Pro:byMe davidtan$ scala 07.scala
Person(First name is :John last name is : Smoith)

 */