
class Person(name: String) {
	val firstName = name.split("\\s+")(0)
	val lastName = name.split("\\s+")(1)

	override def toString = "Person(" + firstName + ", " + lastName + ")"
}

val p = new Person("John Smith")
println(p)

/*
Davids-MacBook-Pro:Chapter05Classes davidtan$ scala 07.scala
Person(John, Smith)

 */