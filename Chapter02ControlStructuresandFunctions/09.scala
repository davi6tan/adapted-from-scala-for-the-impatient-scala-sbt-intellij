def prod(s: String) : Long = {
	if (s.size > 0)
		prod(s.tail) * s.head.toLong
	else
		1
}

println(prod("Hello"));

/*
April 12, 2015

Davids-MacBook-Pro:Chapter02ControlStructuresandFunctions davidtan$ scala 10.scala
2^2=4.000000
10^9=1000000000.000000
3^3=27.000000
2^-1=0.500000
5^0=1.000000

 */