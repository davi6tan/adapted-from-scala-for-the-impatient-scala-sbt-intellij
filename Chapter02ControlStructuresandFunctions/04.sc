def countdown(x: Int) = {
  for (i <- Range(x, 0, -1))
    println(i)

}

countdown(15)

/*
countdown: countdown[](val x: Int) => Unit





15
14
13
12
11
10
9
8
7
6
5
4
3
2
1
res0: Unit = ()

 */