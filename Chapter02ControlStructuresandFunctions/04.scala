def countdown(n: Int) = {
	for( i <- Range(n, 0, -1) )
		println(i)
}

countdown(5);


//scala -Xnojline < 04.scala
//
//Type :help for more information.
//
//scala>      |      |      | countdown: (n: Int)Unit
//
//scala>
//  scala> 5
//4
//3
//2
//1
