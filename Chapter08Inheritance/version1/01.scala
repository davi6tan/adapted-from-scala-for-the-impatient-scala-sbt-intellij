

class BankAccount(initialBalance: Double) {
  private var balance = initialBalance

  def currentBalance = balance

  def deposit(amount: Double) = {
    balance += amount;
    balance
  }

  def withdraw(amount: Double) = {
    balance -= amount;
    balance
  }
}

class CheckingAccount(initialBalance: Double, val commission: Double = 1.0) extends BankAccount(initialBalance) {
  override def deposit(amount: Double) = super.deposit(amount - commission)
  override def withdraw(amount: Double) = super.withdraw(amount + commission)

}

val x = new CheckingAccount(100.0)

x.withdraw(2)

println(x.currentBalance) //97

x.deposit(10)

println(x.currentBalance)//106

val y = new CheckingAccount(200.0, 2.0)

y.withdraw(2)

println(y.currentBalance)//196


y.deposit(10)

println(y.currentBalance)//204

/*
97.0
106.0
204.0

 */

