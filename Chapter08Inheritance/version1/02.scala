
class BankAccount(initialBalance: Double) {
  private var _balance = initialBalance

  def deposit(amount: Double) = {
    _balance += amount; _balance
  }

  def withdraw(amount: Double) = {
    _balance -= amount; _balance
  }

  def balance(): Double = _balance

  override def toString = "Balance = %f".format(balance)
}


class SavingAccount(initialBalance: Double,
                    val interestRateYear: Double = 0.10,
                    val freeTransactions: Int = 3,
                    val comission: Double = 1.0
                     ) extends BankAccount(initialBalance) {

  var transactionMonth: Int = 0

  def isFreeTransaction() = transactionMonth <= freeTransactions

  override def deposit(amount: Double) = {
    transactionMonth += 1
    super.deposit(amount - (if (isFreeTransaction()) 0 else comission))
  }

  override  def withdraw(amount: Double) = {
    transactionMonth += 1
    super.deposit(amount + (if (isFreeTransaction()) 0 else comission))
  }

  def earnMonthlyInterest ={
    transactionMonth = 0 // reset
    super.deposit(balance()*interestRateYear/12)
  }

}

val a = new BankAccount(100.0)

println(a.balance())

a.deposit(12)//112
a.deposit(11)//123
a.withdraw(1)//122
a.deposit(12)//134

println(a.balance())