
//package Chapter08Inheritance

class Person(val name: String) {
	override def toString = getClass.getName + "[name=" + name + "]"
}

class SecretAgent(codename: String) extends Person(codename) {
	override val name = "secret" // Don't want to reveal name...
	override val toString = "secret22" //... or class name
}


val b = new SecretAgent("Flower")

println(b.toString) //secret22
println(b.name.toString) //secret