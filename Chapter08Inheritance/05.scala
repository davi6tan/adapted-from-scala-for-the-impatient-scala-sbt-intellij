

class Point(val x: Int, val y: Double) {
	override def toString = "Point(%d, %f)".format(x, y)
}

class LabeledPoint(val label: String, x: Int, y: Double) extends Point(x, y) {
	override def toString = { "LabeledPoint(%s, %d, %f)".format(label, x, y) }
}

val a = new Point(1, 1)
println(a)  //Point(1, 1.000000)

val b = new LabeledPoint("Black Thursday", 1929, 230.07)  //LabeledPoint(Black Thursday, 1929, 230.070000)
println(b)

/*
Davids-MacBook-Pro:Chapter08Inheritance davidtan$ scala 05.scala
Point(1, 1.000000)
LabeledPoint(Black Thursday, 1929, 230.070000)
Davids-MacBook-Pro:Chapter08Inheritance davidtan$

 */
