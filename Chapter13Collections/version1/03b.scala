
val a = List(0,2,3,5,6,0,0,8,9,0,10,12)


def filterZero(l:List[Int]):List[Int]={
  l.filterNot(_==0)
}

println(filterZero(a))

def filterZeroRecursive(l:List[Int]):List[Int]=l match {
  case Nil => List()
  case h::t => if(h!=0)h::filterZeroRecursive(t)else filterZeroRecursive(t)
}

println(filterZeroRecursive(a))
