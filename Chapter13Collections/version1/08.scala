

def arrayDim(a:Array[Int], numofDim:Int) = a.grouped(numofDim).toArray.map(_.toArray)

val a = (1 to 50).toArray
val x = arrayDim(a,5)

println(x.deep.mkString(","))

/*
Array(1, 2, 3, 4, 5),Array(6, 7, 8, 9, 10),Array(11, 12, 13, 14, 15),Array(16, 17, 18, 19, 20),
Array(21, 22, 23, 24, 25),Array(26, 27, 28, 29, 30),Array(31, 32, 33, 34, 35),Array(36, 37, 38, 39, 40),
Array(41, 42, 43, 44, 45),Array(46, 47, 48, 49, 50)

 */

//val iter = (1 to 50).sliding(5).toArray.deep.mkString(",")
//println()
//println()
//println(iter)