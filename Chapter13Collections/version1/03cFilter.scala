

val a = List(1,3,4,0,7,8,9,12,0)

def filterZero(l:List[Int]):List[Int]={
  l.filterNot(_==0)
}

println(filterZero(a))

def filterZeroRecursive(l:List[Int]): List[Int]=l match{
  case Nil => List()
  case h::t => if(h!=0)h::filterZeroRecursive(t)else filterZeroRecursive(t)
}

println(filterZeroRecursive(a))

/*
Davids-MacBook-Pro:byMe davidtan$ scala 03cFilter.scala
List(1, 3, 4, 7, 8, 9, 12)
List(1, 3, 4, 7, 8, 9, 12)
 */