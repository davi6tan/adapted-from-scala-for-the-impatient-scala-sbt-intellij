
val prices = List(5.0, 20.0, 9.95)
val quantities = List(10, 2, 1)

val a = (prices, quantities).zipped
println(a.mkString(","))
val b = a map {
  _ * _
}
println(b)

/*
Davids-MacBook-Pro:byMe davidtan$ scala 07.scala
(5.0,10),(20.0,2),(9.95,1)
List(50.0, 40.0, 9.95)

 */