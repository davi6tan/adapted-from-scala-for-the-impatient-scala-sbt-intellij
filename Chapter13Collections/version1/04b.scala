

val a = Array("Tom", "Fred", "Harry")
val b = Map("Tom" -> 3, "Dick" -> 4, "Harry" -> 5)


def existIn(a:Array[String], b:Map[String,Int])={
  a.toList.map(b.get(_)).flatMap(x=>x)
}

println(existIn(a,b)) //List(3, 5)
