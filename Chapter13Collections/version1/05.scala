
def mkString[T](s: Seq[T], sep_arator: String = ","): String = {
s.map(_.toString).reduceLeft(_.toString + sep_arator+_.toString)
}

val a = Array("TOm","Marry","John")

println(mkString(a))