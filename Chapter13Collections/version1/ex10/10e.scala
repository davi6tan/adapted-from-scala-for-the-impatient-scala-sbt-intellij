val str = io.Source.fromFile("10.txt").mkString



def printMillis(s:String)(b: =>Unit){
  val start = System.currentTimeMillis()
  b
  val end = System.currentTimeMillis()
  println(s.format(end-start))
}
printMillis("Using immutable collection: %d ms") {
  val frequencies = str.map(c => (c, 1)).groupBy(_._1).map(x=>(x._1,x._2.length))


  println(frequencies.toSeq)
}