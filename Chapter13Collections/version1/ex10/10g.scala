import scala.collection.mutable



val str = io.Source.fromFile("mary.txt").mkString
val hist = new mutable.HashMap[Char,Int]()
for(c<-str)hist(c)= hist.getOrElse(c,0)+1
println(hist.toSeq.sorted)

val hist2 = str.map(c=>(c,1)).groupBy(_._1).map(x=>(x._1,x._2.length))
println(hist2.toSeq.sorted)

