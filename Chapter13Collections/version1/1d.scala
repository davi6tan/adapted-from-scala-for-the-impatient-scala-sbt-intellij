import scala.collection.mutable


def indexes(s:String)={
  val result = new mutable.HashMap[Char,mutable.LinkedHashSet[Int]]()
  for((c,index)<-s.zipWithIndex){
    val set = result.getOrElse(c.toChar,new mutable.LinkedHashSet[Int])
    set+=index
    result(c.toChar) = set
  }

  result
}

val x = "Mississippi"

println(indexes(x))
/*
Davids-MacBook-Pro:byMe davidtan$ scala 1d.scala
Map(M -> Set(0), s -> Set(2, 3, 5, 6), p -> Set(8, 9),
i -> Set(1, 4, 7, 10))

 */
