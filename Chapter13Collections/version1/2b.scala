

def indexes(s:String)={
  val result = s.zipWithIndex.
    groupBy(_._1).
    map(x=> (x._1,
              x._2.map(_._2).toList))


  result
}

val x = "Mississippi"

println(indexes(x).toSeq)

