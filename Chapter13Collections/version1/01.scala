import scala.collection.mutable.{HashMap, LinkedHashSet}




//def indexes(s: String) = {
//  var out = new HashMap[Char, LinkedHashSet[Int]]()
//  for ((c, i) <- s.zipWithIndex) {
//    println("C is", c, "i is", i)
//    val set = out.getOrElse(c.toChar, new LinkedHashSet[Int])
//    set += i
//    out(c.toChar) = set
//  }
//    out
//}
def indexes(s: String) = {
  var res = new HashMap[Char, LinkedHashSet[Int]]()

  for ((c, i) <- s.zipWithIndex) {
    val set = res.getOrElse(c.toChar, new LinkedHashSet[Int])
    println(" Top Set ", set,"I ",i)
    set += i  //append into set
    res(c.toChar) = set
    println("Char is ", c, " Set location ", set)
  }

  res
}
val a = "Mississippi"

val result = indexes(a)

println(result)