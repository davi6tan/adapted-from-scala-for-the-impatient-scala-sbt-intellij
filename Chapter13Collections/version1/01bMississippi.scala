


import scala.collection.mutable

def indexes(s:String)={
  val res = new mutable.HashMap[Char, mutable.LinkedHashSet[Int]]()
  for((c,i)<-s.zipWithIndex){ // i is index
    val set = res.getOrElse(c.toChar, new mutable.LinkedHashSet[Int])
//    println("I is",i)
    set +=i
    res(c.toChar) = set
    //println(res)
  }
  res
}

val x = "Mississippi"

println()
println()
println(x)
println(indexes(x))
