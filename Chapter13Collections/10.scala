
val str = io.Source.fromFile("10.txt").mkString

def printMills(msg: String)(block: => Unit) {
	val start = System.currentTimeMillis()
	block
	val end = System.currentTimeMillis()
	println(msg.format(end-start))
}

printMills("Using mutable collection: %d ms") {
	val freq = new collection.mutable.HashMap[Char, Int] 
	for (c <- str) freq(c) = freq.getOrElse(c, 0) + 1
	//println(freq.toSeq.sorted)
}


printMills("Using immutable collection: %d ms") {
	val freq = str.map(c => (c, 1)).groupBy(_._1).map(x => (x._1, x._2.length))
	//println(freq.seq.toSeq.sorted)
}

printMills("Using mutable parallel collection: %d ms") {

	val freq = str.par.aggregate(new collection.immutable.HashMap[Char, Int])(
		(x, c) => x + (c ->(x.getOrElse(c, 0) + 1)),
		(map1, map2) => map1 ++ map2.map{ case (k,v) => k -> (v + map1.getOrElse(k,0)) }		
	)

	//println(freq.toSeq.sorted)
}
/*
Davids-MacBook-Pro:Chapter13Collections davidtan$ scala 10.scala

ArrayBuffer((
,674), ( ,5835), (",82), (',24), ((,45), (),60), (,,313), (-,24), (.,218), (/,20), (0,14), (1,28), (2,13), (3,9),
(4,5), (5,5), (6,8), (7,8), (8,2), (9,4), (:,11), (;,17), (<,10), (>,10), (A,124), (B,22), (C,78), (D,49), (E,122),
(F,46), (G,69), (H,46), (I,129), (J,1), (K,3), (L,141), (M,33), (N,99), (O,94), (P,104), (Q,3), (R,106), (S,104),
(T,144), (U,60), (V,13), (W,23), (X,3), (Y,48), (`,4), (a,1793), (b,300), (c,1087), (d,870), (e,3104), (f,663),
(g,456), (h,1013), (i,2037), (j,27), (k,174), (l,800), (m,623), (n,1803), (o,2505), (p,672), (q,32), (r,2073), (s,1576),
(t,2300), (u,764), (v,314), (w,392), (x,53), (y,598), (z,11))

Using mutable collection: 24 ms
ArrayBuffer((
,674), ( ,5835), (",82), (',24), ((,45), (),60), (,,313), (-,24), (.,218), (/,20), (0,14), (1,28), (2,13), (3,9),
(4,5), (5,5), (6,8), (7,8), (8,2), (9,4), (:,11), (;,17), (<,10), (>,10), (A,124), (B,22), (C,78), (D,49), (E,122),
(F,46), (G,69), (H,46), (I,129), (J,1), (K,3), (L,141), (M,33), (N,99), (O,94), (P,104), (Q,3), (R,106), (S,104), (T,144),
(U,60), (V,13), (W,23), (X,3), (Y,48), (`,4), (a,1793), (b,300), (c,1087), (d,870), (e,3104), (f,663), (g,456), (h,1013),
(i,2037), (j,27), (k,174), (l,800), (m,623), (n,1803), (o,2505), (p,672), (q,32), (r,2073), (s,1576), (t,2300), (u,764), (v,314), (w,392), (x,53), (y,598), (z,11))
Using immutable collection: 22 ms

ArrayBuffer((
,674), ( ,5835), (",82), (',24), ((,45), (),60), (,,313), (-,24), (.,218), (/,20), (0,14), (1,28),
 (2,13), (3,9), (4,5), (5,5), (6,8), (7,8), (8,2), (9,4), (:,11), (;,17), (<,10), (>,10), (A,124),
 (B,22), (C,78), (D,49), (E,122), (F,46), (G,69), (H,46), (I,129), (J,1), (K,3), (L,141), (M,33), (N,99),
  (O,94), (P,104), (Q,3), (R,106), (S,104), (T,144), (U,60), (V,13), (W,23), (X,3), (Y,48), (`,4),
   (a,1793), (b,300), (c,1087), (d,870), (e,3104), (f,663), (g,456), (h,1013), (i,2037), (j,27), (k,174),
    (l,800), (m,623), (n,1803), (o,2505), (p,672), (q,32), (r,2073), (s,1576), (t,2300), (u,764), (v,314),
     (w,392), (x,53), (y,598), (z,11))

Using mutable parallel collection: 71 ms
Davids-MacBook-Pro:Chapter13Collections davidtan$

 */