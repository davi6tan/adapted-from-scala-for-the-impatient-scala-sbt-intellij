package Chapter10Traits.byMe.others

abstract class Fruit extends Tree{
  override def size()=1
  override def grow(f:Int):Tree = new Cluster(f,this)

}
trait Tree{
  def size():Int
  def grow(f:Int):Tree
  def harvest(bag:Fruit*)//harvest(Collection<Fruit> bag);


//  def getX(): Double
//  def getY(): Double
//  def getWidth(): Double
//  def getHeight(): Double
//  def setFrame(x: Double, y: Double, width: Double, height: Double)
//
//  def translate(dx: Double, dy: Double) = {
//    setFrame(getX() + dx, getY() + dy, getWidth(), getHeight())
//  }

//  def grow(dx: Double, dy: Double) = {
//    setFrame(getX() - dx, getY() - dy, getWidth() + 2 * dx, getHeight() + 2 * dy)
//  }

//  override def toString = "[%f, %f, %f, %f]".format(getX(), getY(), getWidth(), getHeight())

}
class Cluster(howmany:Int,child:Tree) extends Tree{
  override def size()=1
  override def grow(f:Int):Tree = new Cluster(f,this)
  override def harvest(bag:Fruit*) = bag//??
}