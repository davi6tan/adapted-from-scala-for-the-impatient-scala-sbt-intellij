trait Animal {
  val name: String

  def words: String = ""

  def say = println("%s: %s".format(name, words))
}

trait Tree {
  def size: Int

  def grow(factor: Int): Tree

  def harvest: List[Fruit]
}

abstract class Fruit extends Tree {
  override val size = 1

  override def grow(factor: Int) = new Cluster()
}

class Cluster extends Tree {
  val howmany: Int
  val child: Tree

  def apply(howmany: Int, child: Tree*): Unit ={

  }

  def getChild: Tree = child

  def getMultiplier: Int = howmany

  override val size = getMultiplier
  def grow = getChild

  //def harvest = AnyRef
}


trait Endothermy

trait Mammal extends Animal with Endothermy

class Dog extends Mammal {
  val name = "Scooby"
  override val words = "BowWow"

  override def say = {
    print("Dog name is:")
    super.say
  }
}

class Cat extends Mammal {
  val name = "Tom"
  override val words = "MeowScratchScratch"

  override def say = {
    print("Cat name is:")
    super.say
  }
}

val d = new Dog()
val c = new Cat()

d.say
c.say
