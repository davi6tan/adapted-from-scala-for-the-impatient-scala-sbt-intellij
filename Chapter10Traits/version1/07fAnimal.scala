
trait Animal {
  val name: String

  // abstract method
  def words: String

  def say = "%s: and my sound is {%s}".format(name, words)
}

trait Endothermy

// abstract trait

trait Mammal extends Animal with Endothermy

class Cat extends Mammal {
  val name = "Mimmy"
  override val words = "Meozwe"

  override def say = {
    print("I am a Cat and my name is ")
    super.say
  }
}

class Dog extends Mammal {
val name ="Scoooby"
  override val words = "ScoobyDoooDOoo"
}

val c = new Cat()

println(c.say)


val d = new Dog

println(d.say)