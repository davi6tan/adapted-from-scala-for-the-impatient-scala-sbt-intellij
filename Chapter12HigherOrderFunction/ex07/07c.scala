


def adjustToPair(f: (Int, Int) => Int) = (x: (Int, Int)) => (f(x._1, x._2))


val x = adjustToPair(_ + _)(6, 7)

println(x) //13


val pairs = (1 to 10) zip (10 to 20)

println(pairs)


val y = pairs.map(adjustToPair(_ + _))

println(y)




def adjusttoPair2(f: (Int, Int) => Int) = (x: (Int, Int)) => (f(x._1, x._2))

val z = pairs.map(adjusttoPair2(_ * 2 + _))


println(z)



