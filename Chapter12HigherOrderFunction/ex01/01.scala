def values(fun: (Int) => Int, low: Int, high: Int) = (low to high) map { x => (x, fun(x)) }

val result = values(x => x * 2, -10, 10)

println("Range with anonymous function:", (-10 to 10) map { x => (x, x * 2) })
/*
(Range with anonymous function:,Vector((-10,-20), (-9,-18), (-8,-16), (-7,-14), (-6,-12),
(-5,-10), (-4,-8), (-3,-6), (-2,-4), (-1,-2), (0,0), (1,2), (2,4), (3,6), (4,8), (5,10),
 (6,12), (7,14), (8,16), (9,18), (10,20)))
 */
println("Result is: ", result)

/*
(Result is: ,Vector((-10,-20), (-9,-18), (-8,-16), (-7,-14), (-6,-12), (-5,-10),
(-4,-8), (-3,-6), (-2,-4), (-1,-2), (0,0), (1,2), (2,4), (3,6), (4,8), (5,10),
(6,12), (7,14), (8,16), (9,18), (10,20)))
 */