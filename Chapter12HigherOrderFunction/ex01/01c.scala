def values(fun: (Int) => Int, low: Int, high: Int) = (low to high).map(x => (x, fun(x)))
val a = values(x => x * x, 1, 10)
println(a)
//Vector((1,1), (2,4), (3,9), (4,16), (5,25), (6,36), (7,49), (8,64), (9,81), (10,100))

def values2(fun: (Int) => Int, low: Int, high: Int) = {
  for (x <- low to high) yield Pair(x, fun(x))
}

val b = values2(x => x * x, 1, 10)
println(b)
//Vector((1,1), (2,4), (3,9), (4,16), (5,25), (6,36), (7,49), (8,64), (9,81), (10,100))

/*
Vector((1,1), (2,4), (3,9), (4,16), (5,25), (6,36), (7,49), (8,64), (9,81), (10,100))
Vector((1,1), (2,4), (3,9), (4,16), (5,25), (6,36), (7,49), (8,64), (9,81), (10,100))
Davids-MacBook-Pro:ex01 davidtan$
 */