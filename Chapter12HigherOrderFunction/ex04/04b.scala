

def factorial1(x:Int):Int = (1 to x).foldLeft(1)(_*_)
def factorial2(x:Int):Int = (1 to x).reduceLeft(_*_)

println(factorial1(5))//120
println("FACTORIAL 2",factorial2(5))//120