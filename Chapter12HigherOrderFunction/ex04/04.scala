
def factorial(x: Int): Int = (1 to x).foldLeft(1)(_ * _)

println(factorial(5))//120