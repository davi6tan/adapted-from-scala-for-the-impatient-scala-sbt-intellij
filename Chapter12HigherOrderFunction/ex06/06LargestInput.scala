

def largest05(fun: (Int) => Int, inputs: Seq[Int]) = {
  inputs.map(fun(_)).max
}

val fun05 = largest05(x => 10 * x - x * x, 1 to 10)

println(fun05) //25
// map convert to tuple
def largest06(fun: (Int) => Int, inputs: Seq[Int]) = {
  inputs.map(x => (x, fun(x))).reduceLeft((l, r) => if (l._2 > r._2) l else r) //._1
}
val fun06 = largest06(x => 10 * x - x * x, 1 to 10)
println(fun06) //(5,25)