def getLargest(l: List[Int]): Int = l.reduceLeft((x, y) => if (x > y) x else y)

val list = List(1, 4, 5, 6, 9, 12, 128, 1, 0, 9)

println(getLargest(list))


def largestAt(f: (Int) => Int, inputs: Seq[Int]) = {
  inputs.map(a => (a, f(a))).reduceLeft((l, r) => if (l._2 > r._2) l else r)
}

println(largestAt((x => x * x + x + 1), (1 to 10)))

