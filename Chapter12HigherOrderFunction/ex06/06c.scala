
def largestAt(fun: (Int) => Int, inputs: Seq[Int]) = inputs.map(x=>(x,fun(x))).reduceLeft((l,r)=>if(l._2>r._2)l else r)


println(largestAt((x=> x*x+1 - 2*x),(1 to 10)))