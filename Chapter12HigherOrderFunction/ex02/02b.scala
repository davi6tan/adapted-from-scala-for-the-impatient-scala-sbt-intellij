import scala.util.Random

val x = new Array[Int](10).map(a=>Random.nextInt(100))

println(x.mkString(","))


def getMax(arr:Array[Int]):Int ={
  arr.reduceLeft((first,next)=>if(first > next) first else next)
}

println(getMax(x))

/*
45,29,0,70,11,57,27,51,50,96
96

 */