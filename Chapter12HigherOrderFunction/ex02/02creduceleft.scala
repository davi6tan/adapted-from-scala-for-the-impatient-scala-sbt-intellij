import scala.util.Random

val a = new Array[Int](10).map(x=>Random.nextInt(100))

println(a.mkString(","))


val result = a.reduceLeft((l,r)=>if(l>r)l else r)

println(result)