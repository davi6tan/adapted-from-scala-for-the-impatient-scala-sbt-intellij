
val a = Array("HelloL", "forMulaOne1", "BlackHwaks")

val b = a.map(_.length)

println(b.mkString(","))

val merge = a zip b

println(merge.mkString(","))

def corresponds[A, B](a: Seq[A], b: Seq[B], fun: (A, B) => Boolean) = {
  (a zip b).map(x => fun(x._1, x._2)).count(!_) == 0
}

val isResult09 = corresponds(a, b, (x: String, y: Int) => x.length == y)

println("Result 09 ", isResult09)

/*
Davids-MacBook-Pro:byMe davidtan$ scala 09.scala
6,11,10
(HelloL,6),(forMulaOne1,11),(BlackHwaks,10)
(Result 09 ,true)

 */