

val things = Map("MacKeyBoard"->79,"Mouse"->12,"AcerScreen"->110)

def prn(x:TraversableOnce[_])=println(x.mkString(x.getClass.getSimpleName + "(", ", ", ")"))


val discounted = for((k,v)<- things)yield(k,v*0.9)

prn(things)

prn(discounted)