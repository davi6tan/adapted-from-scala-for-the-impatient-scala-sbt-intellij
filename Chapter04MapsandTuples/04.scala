
import scala.io.Source

def prn(x: TraversableOnce[_]) = println(x.mkString(x.getClass.getSimpleName + "(", ", ", ")"))

val source = Source.fromFile("gpl.txt", "UTF-8")
val tokens = source.mkString.split("\\s+")

var freq = new scala.collection.immutable.TreeMap[String, Int]

tokens foreach { token =>
	freq = freq + (token -> (freq.getOrElse(token, 0) + 1) )
}

prn(freq)

/*
Davids-MacBook-Pro:Chapter04MapsandTuples davidtan$ scala 04.scala
TreeMap( -> 1, "AS -> 1, "Additional -> 1, "Appropriate -> 1, "Copyright" -> 1, "Corresponding -> 1, "Installation -> 1,
"Knowingly -> 1, "Licensees" -> 1, "Major -> 1, "Object -> 1, "Standard -> 1, "System -> 1, "The -> 1, "This -> 1, "User -> 1,
"about -> 1, "aggregate" -> 1, "based -> 1, "consumer -> 1, "contributor -> 1, "contributor" -> 1, "control" -> 1, "convey" -> 1, "copyright -> 1, "copyright" -> 1, "covered -> 1, "discriminatory" -> 1, "entity -> 1, "essential -> 1, "further -> 1, "grant" -> 1, "keep -> 1, "modified -> 1, "modi
 */