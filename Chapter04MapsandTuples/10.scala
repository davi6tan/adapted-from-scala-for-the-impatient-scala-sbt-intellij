val caseConverter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".zip("abcdefghijklmnopqrstuvwxyz").toMap
println(caseConverter)

/*
Map(E -> e, X -> x, N -> n, T -> t, Y -> y, J -> j, U -> u, F -> f, A -> a, M -> m, I -> i, G -> g, V -> v, Q -> q,
 L -> l, B -> b, P -> p, C -> c, H -> h, W -> w, K -> k, R -> r, O -> o, D -> d, Z -> z, S -> s)
d

 */
var lower = caseConverter('D')

println(lower)

/*
d
 */