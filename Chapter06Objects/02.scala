
class UnitConversion(val factor: Double) {
	def convert(value: Double): Double = factor * value
}

object InchesToSantimeters extends UnitConversion(2.54)
object GallonsToLiters extends UnitConversion(3.78541178)
object MilesToKilometers extends UnitConversion(1.609344)

println("%f inch = %f santimeters".format(1.0, InchesToSantimeters.convert(1.0)))
println("%f gallon = %f liters".format(1.0, GallonsToLiters.convert(1.0)))
println("%f mile = %f kilometers".format(1.0, MilesToKilometers.convert(1.0)))

/*
Davids-MacBook-Pro:Chapter06Objects davidtan$ scala 02.scala
1.000000 inch = 2.540000 santimeters
1.000000 gallon = 3.785412 liters
1.000000 mile = 1.609344 kilometers
Davids-MacBook-Pro:Chapter06Objects davidtan$
 */