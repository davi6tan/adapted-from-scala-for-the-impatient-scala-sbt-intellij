
class Point(x: Int = 0, y: Int = 0) extends java.awt.Point(x, y)

object Point {
	def apply(x: Int = 0, y: Int = 0) = new Point(x, y)
}

println(Point(3,4))


/*
Davids-MacBook-Pro:Chapter06Objects davidtan$ scala 04.scala
Main$$anon$1$Point[x=3,y=4]

 */