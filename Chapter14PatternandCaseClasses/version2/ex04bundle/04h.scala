///// july 18 /////

abstract class Item

case class Article(desc: String, price: Double) extends Item

case class Multiple(quantity: Int, item: Item) extends Item

case class Bundle(desc: String, discount: Double, items: Item*) extends Item


def price(item: Item): Double = item match {
  case Article(_, p) => p
  case Multiple(q, item) => price(item) * q
  case Bundle(_, disc, it@_*) => it.map(price _).sum - disc
}

val x = Bundle("Father's day special", 20.0, Multiple(2, Article("Scala for the Impatient", 39.95)),
  Bundle("Anchor Distillery Sampler", 10.0, Article("Old Potrero Straight Rye Whiskey", 79.95), Article("Junípero Gin", 32.95)))

val result = price(x)

println(result) //162.8
