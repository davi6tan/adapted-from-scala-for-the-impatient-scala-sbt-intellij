

abstract class Item

case class Bundle(s: String, price: Double, items: Item*) extends Item

case class Article(desc: String, p: Double) extends Item

case class Multiple(qty: Int, item: Item) extends Item

def price(x: Item): Double = x match {
  case Article(_, p) => p
  case Multiple(q, item) => price(item) * q
  case Bundle(_, discount, items@_*) => items.map(price _).sum - discount

}


val x = Bundle("Father's day special", 20.0,
  Multiple(2, Article("Scala for the Impatient", 39.95)),
  Bundle("Anchor Distillery Sampler", 10.0, Article("Old Potrero Straight Rye Whiskey", 79.95), Article("Junípero Gin", 32.95)))

println(x)
println(price(x)) //162.80