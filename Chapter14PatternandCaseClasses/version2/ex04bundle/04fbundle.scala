
abstract class Item
case class Article(desc:String, price:Double) extends Item
case class Multiple(qty:Int, item:Item) extends Item
case class Bundle(desc:String, discount:Double, items:Item*) extends Item

def price(item:Item):Double = item match {
  case Article(_,p)=>p
  case Multiple(q,item)=>price(item)*q
  case Bundle(_,d,items @_*)=> items.map(price _).sum - d

}
//-20+ 39.95*2 -10+ 79.95 + 32.95
// 2* 39.95
val x = Bundle("Father's day special", 20.0,
  Multiple(2, Article("Scala for the Impatient", 39.95)),
  Bundle("Anchor Distillery Sampler", 10.0,
    Article("Old Potrero Straight Rye Whiskey", 79.95), Article("Junípero Gin", 32.95)))


println(x)

println(price(x))