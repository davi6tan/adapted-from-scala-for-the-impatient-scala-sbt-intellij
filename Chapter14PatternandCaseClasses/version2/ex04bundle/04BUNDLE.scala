

abstract class Item

case class Article(desc: String, p: Double) extends Item

case class Bundle(desc: String, p: Double, item: Item*) extends Item

case class Multiple(count: Int, item: Item) extends Item

def price(_item: Item): Double = _item match {
  case Article(_, p) => p
  case Bundle(_, discount, _items@_*) => _items.map(price _).sum - discount
  case Multiple(qty, this_item) => price(this_item) * qty
}

val x = Bundle("Father's day special", 20.0,
  Multiple(2, Article("Scala for the Impatient", 39.95)),
  Bundle("Anchor Distillery Sampler", 10.0, Article("Old Potrero Straight Rye Whiskey", 79.95), Article("Junípero Gin", 32.95)))

println(x)

println(price(x))


/*
-20+ 39.95*2 -10+ 79.95 + 32.95

= 162.80
 */