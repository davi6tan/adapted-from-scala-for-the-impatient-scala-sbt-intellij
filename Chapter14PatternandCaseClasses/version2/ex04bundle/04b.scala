

abstract class Item

case class Article(desc: String, price: Double) extends Item

case class Bundle(desc: String, discount: Double, items: Item*) extends Item

case class Multiple(count: Int, item: Item) extends Item


def price(item: Item): Double = item match{
  case Article(_, p) => p
  case Bundle(_, discount, leafs@_*) => leafs.map(price _).sum - discount
  case Multiple(c, item) => c * price(item)
}

val x = Bundle("Father's day special", 20.0,
  Multiple(2, Article("Scala for the Impatient", 39.95)),
  Bundle("Anchor Distillery Sampler", 10.0, Article("Old Potrero Straight Rye Whiskey", 79.95), Article("Junípero Gin", 32.95)))

println(x)

println(price(x))
/*
Bundle(Father's day special,20.0,WrappedArray(Multiple(2,Article(Scala for the Impatient,39.95)), Bundle(Anchor Distillery Sampler,10.0,WrappedArray(Article(Old Potrero Straight Rye Whiskey,79.95), Article(Junípero Gin,32.95)))))
162.8
Davids-MacBook-Pro:byMe02 davidtan$

 */