/*


 lst2 ::: 1st  given list prepend to another lst
 */



def leafSumImpl(total:Int, nodes:List[Any]):Int = nodes match{
  case List()=>0
  case head :: tail => head match{
    case x:Int => leafSumImpl(x+total, tail)
    case y:List[Any]=> leafSumImpl(total, y:::tail)
    case _ => total
  }
}

def leafsum(l:List[Any]):Int ={
  val result =leafSumImpl(0,l)
  result

}
val list1 = List(List(3,8),2,List(5))