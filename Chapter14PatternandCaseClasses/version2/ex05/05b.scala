/*
May 23, 2015
*/

private def leafSumImpl(total: Int, nodes: List[Any]): Int = nodes match {
  case List() => total
  case head :: tail => head match {
    case x: Int => leafSumImpl(x + total, tail)
    case y: List[Any] => leafSumImpl(total, y ::: tail)
    case _ => total
  }
}

def leafSum(tree:List[Any]):Int ={
  val result = leafSumImpl(0,tree)
  result
}

val data = List(List(3,8),2,List(5))//18


println(leafSum(data))


val data2 = List(1,List(3,4,5),List(6),5,6,7,List(3,5,0))//45

println(leafSum(data2))