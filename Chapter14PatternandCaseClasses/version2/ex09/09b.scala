

def sumList(l:List[Option[Int]]):Int = l.map(_.getOrElse(0)).sum

val x = List(Some(1),Some(5),None,Some(6))

println(sumList(x))//12