

def sum(lst: List[Option[Int]]) = lst.map(_.getOrElse(0)).sum

val x = List(Some(1), None, Some(2), None, Some(3))

println(x)

println(sum(x))//6

val y = List(Some(1),Some(2),None,Some(6))

println(sum(y))