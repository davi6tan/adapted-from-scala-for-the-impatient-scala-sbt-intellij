/*


************************************************************************
http://mbonaci.github.io/scala/
240 - ++ operator is used to concatenate two arrays

 349 - First-order methods on class List
a method is first order if it doesn't take any functions as arguments
Concatenating two lists
xs ::: ys returns a new list that contains all the elements of xs, followed by all the elements of ys


normally, infix notation (e.g. x :: y) is equivalent to a method call, but with patterns, rules are different.
When seen as a pattern, an infix operator is treated as a constructor:
x :: y is equivalent to ::(x, y) (not x.::(y))
there is a class named ::, scala.:: (builds non-empty lists)
there is also a method :: in class List (instantiates class scala.::)

**************************************************************************
if you want the function to match cases like the last two, you'll have to match
against a sequence of nodes instead of a single one
the pattern for any sequence is written _*:
May 22, 2015
 */

def swap3(arr: Array[Int]) = arr match {
  case Array(x, y, xs@_*) => Array(y, x) ++ xs
  case _ => arr
}

val orig = Array(1, 3, 4, 5)

val result = swap3(orig)

println(orig.mkString(","))

println(result.mkString(","))