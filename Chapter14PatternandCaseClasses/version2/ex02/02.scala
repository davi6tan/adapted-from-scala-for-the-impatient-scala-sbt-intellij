def swap(p: Tuple2[Int, Int]) = p match {
  case (x, y) => (y, x)
}

val orig = (3, 4)

val result = swap(orig)

println("ORIG", orig)
println("Result", result)