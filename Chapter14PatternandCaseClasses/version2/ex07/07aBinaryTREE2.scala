/*
7.
 */

sealed abstract class BinaryTree

case class Leaf(value: Int) extends BinaryTree

case class Node(tree: BinaryTree*) extends BinaryTree

def leafsum(tree: BinaryTree): Int = tree match {
  case Node(leafs@_*) => leafs.map(leafsum _).sum
  case Leaf(x) => x
}

val data = Node(Node(Leaf(1), Leaf(5)), Node(Leaf(3), Leaf(9)))

println(leafsum(data)) //18

val data2 = Node(Leaf(1), Leaf(5))

println(leafsum(data2)) //6

val data3 = Node(Node(Leaf(2), Leaf(1), Leaf(5), Node(Leaf(9), Node(Leaf(7)))))

println(leafsum(data3)) //24