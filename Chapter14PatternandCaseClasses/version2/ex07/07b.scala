/*
7.
 */

sealed abstract class BinaryTree

case class Leaf(value: Int) extends BinaryTree

case class Node(trees:BinaryTree*) extends BinaryTree

def leafsum(tree: BinaryTree): Int = tree match {
  case Node(trees @_*) => trees.map(leafsum _).sum
  case Leaf(x) => x
}
val tree01 = Node(Leaf(12), Node(Node(Leaf(3), Leaf(8)), Node(Leaf(5), Leaf(9))))

println(tree01)

println(leafsum(tree01)) //37