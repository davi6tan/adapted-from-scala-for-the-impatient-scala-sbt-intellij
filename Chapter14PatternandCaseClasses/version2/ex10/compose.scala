def compose(f:Int=>Int,g:Int=>Int):Int=>Int= x=>f(g(x))
def sqr(x:Int)=x*x
def cube(y:Int)= y*y*y

val result = compose(sqr,cube) // 8^2 = 64

println(result(2))

val result2 = compose(cube,sqr) // 4^3 = 64
println(result2(2))


//http://www.slideshare.net/pramode_ce/introduction-to-functional-programming-with-scala -pg 25