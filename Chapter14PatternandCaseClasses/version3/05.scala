/**
 * Created by davidtan on 4/20/15.
 */
private def leafSumImpl(total: Int, nodes: List[Any]): Int = nodes match {
  case List() => total // empty list
  case head :: tail => head match {
    case x: Int => leafSumImpl(x + total, tail)
    case y: List[Any] => leafSumImpl(total, y ::: tail)
    case _ => total
  }
}

def leafsum(trees: List[Any]): Int = {
  val result = leafSumImpl(0, trees)
  result
}


val sample = List(List(3, 8), 2, List(5))


println(leafsum(sample))

/*
Davids-MacBook-Pro:github davidtan$ scala 05.scala
18

 */