
def swap(p: Tuple2[Int, Int]) = p match {
  case (x, y) => (y, x)
}

val x = (1, 2)
val y = swap(x)

println(x)
println(y)