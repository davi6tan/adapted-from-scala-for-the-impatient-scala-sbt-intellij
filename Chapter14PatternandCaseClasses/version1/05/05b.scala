/**
 * Created by davidtan on 4/19/15.
 */



private def leafsumImpl(total:Int,nodes:List[Any]):Int= nodes match{
  case List()=>total
  case head::tail => head match {
    case x: Int =>leafsumImpl(x+total,tail)
    case y:List[Any]=>leafsumImpl(total, y:::tail)
    case _ =>total
  }
}

def leafsum(tree:List[Any]):Int ={
  val result = leafsumImpl(0,tree)
  result
}

val sample = List(List(3,8),2,List(5))

println(leafsum(sample))