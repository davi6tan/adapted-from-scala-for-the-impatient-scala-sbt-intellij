/**
 * Created by davidtan on 4/19/15.
 */
/*

http://mbonaci.github.io/scala/
***********************
  136 - Associativity
  *********************
any method that ends in a : character is invoked on its right operand, passing in the left operand.
 Methods that end in any other character are invoked on their left operand, passing in the right operand.
 So a * b yields a.*(b), but a ::: b yields b.:::(a)
137 - a ::: b ::: c is treated as a ::: (b ::: c) (list concatenation)
 */
/**
 * (14.5)
 */
private def leafSumImpl(total:Int, nodes:List[Any]): Int = {
  nodes match {
    case List() => total
    case head::tail =>
      head match {
        case x:Int => leafSumImpl(x + total, tail)
        case y:List[Any] => leafSumImpl(total, y ::: tail)
        case _ => total
      }
  }
}
def leafSum(tree:List[Any]): Int = {
  val result = leafSumImpl(0, tree)
  result
}

val x = List(List(3, 8), 2, List(5))

println(x)

println(leafSum(x))

/*
Davids-MacBook-Pro:byMe davidtan$ scala 05a.scala
List(List(3, 8), 2, List(5))
18
Davids-MacBook-Pro:byMe davidtan$

 */