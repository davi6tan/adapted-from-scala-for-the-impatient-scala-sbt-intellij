/**
 * Created by davidtan on 4/19/15.
 */


def swap(pair:Tuple2[Int,Int])= pair match{
  case (x,y)=>(y,x)
}

println(swap(1,8))

/*
Davids-MacBook-Pro:byMe davidtan$ scala 02.scala
(8,1)
David
 */
