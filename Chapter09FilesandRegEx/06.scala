
val source = io.Source.fromFile("06.txt")
val pattern = """"([^"\\]*([\\]+"[^"\\]*)*)"""".r

for(str <- source.getLines) {  //io.Source.stdin
	pattern findFirstIn str match {
		case Some(s: String) => println(s)
		case None =>
	}
}
/*
"test2"
"test\" 5"
"test \\" 8"


============
test1 "test2" test3
test4 "test\" 5" test6
test7 "test \\" 8" test9



/([^"\\]*([\\]+"[^"\\]*)*)/
1st Capturing group

([^"\\]
*
([\\]+
"
[^"\\]*)*)

1)[^"\\]* match a single character not present in the list below
Quantifier: * Between zero and unlimited times, as many times as possible, giving back as needed [greedy]
" a single character in the list " literally (case sensitive)
\\ matches the character \ literally

2)2nd Capturing group ([\\]+"[^"\\]*)*
Quantifier: * Between zero and unlimited times, as many times as possible, giving back as needed [greedy]
Note: A repeated capturing group will only capture the last iteration. Put a capturing group around the repeated group to capture all iterations or use a non-capturing group instead if you're not interested in the data

3)[\\]+ match a single character present in the list below
Quantifier: + Between one and unlimited times, as many times as possible, giving back as needed [greedy]
\\ matches the character \ literally

4)" matches the characters " literally

5)[^"\\]* match a single character not present in the list below
Quantifier: * Between zero and unlimited times, as many times as possible, giving back as needed [greedy]

6)" a single character in the list " literally (case sensitive)
7) \\ matches the character \ literally

 */
//http://www.scala-lang.org/api/2.11.4/index.html#scala.util.matching.Regex