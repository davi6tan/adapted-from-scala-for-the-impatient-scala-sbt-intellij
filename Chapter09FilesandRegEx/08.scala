

val html = io.Source.fromURL("http://horstmann.com", "UTF-8").mkString

val srcPattern = """(?is)<\s*img[^>]*src\s*=\s*['"\s]*([^'"]+)['"\s]*[^>]*>""".r

for(srcPattern(s) <- srcPattern findAllIn html) println(s)

/*
Davids-MacBook-Pro:Chapter09FilesandRegEx davidtan$ scala 08.scala

images/cay-rowing.gif
images/violet.jpg
images/zork1.gif


Davids-MacBook-Pro:Chapter09FilesandRegEx davidtan$

 */
