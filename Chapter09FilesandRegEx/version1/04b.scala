
val source = io.Source.fromFile("04.txt")

val numbers = source.mkString.split("""\s+""").map(_.toDouble)
//val numbers = source.mkString.split("\\s+").map(_.toDouble)  this work too..


println("Numbers are ", numbers.mkString(", "))
println(numbers.toSeq)

println("Sum ", numbers.sum)
println("Average ", numbers.sum / numbers.length)
println("Maximum ", numbers.max)
println("Minimum ", numbers.min)



/*
avids-MacBook-Pro:byMe davidtan$ scala 04b.scala
(Numbers are ,1.0, 2.0, 3.0, 4.0, 6.6, 333.1, 33.0)
WrappedArray(1.0, 2.0, 3.0, 4.0, 6.6, 333.1, 33.0)
(Sum ,382.70000000000005)
(Average ,54.67142857142858)
(Maximum ,333.1)
(Minimum ,1.0)

 */