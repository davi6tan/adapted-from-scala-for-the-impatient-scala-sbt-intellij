

val numbers = io.Source.
  fromFile("04.txt").mkString.
  split("""\s+""").map(_.toDouble)

println("Numbers: " + numbers.mkString(", "))

println("Sum: " + numbers.sum)
println("Avg: " + numbers.sum / numbers.length)
println("Min: " + numbers.min)
println("Max: " + numbers.max)



/*


Numbers: 1.0, 2.0, 3.0, 4.0, 6.6, 333.1, 33.0
Sum: 382.70000000000005
Avg: 54.67142857142858
Min: 1.0
Max: 333.1



*********
04.txt
*********
1
2
3
4 6.6 333.1
33


 */