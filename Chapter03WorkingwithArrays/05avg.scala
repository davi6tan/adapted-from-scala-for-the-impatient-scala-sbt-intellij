
import util.Random

def randomArray(n: Int) = {
	val a = new Array[Double](n);
	for (i <- 0 until a.size) a(i) = Random.nextDouble;
	a
}

def avg(a: Array[Double]) = a.sum / a.size;

val test = randomArray(1000000);
val res = avg(test);

println(test.mkString("Array(", ", ", ")"))
/*
....

8719705235, 0.8889821931152746, 0.46501070850246984, 0.5167437798202776, 0.06281876617598203, 0.884062227554931,
0.07097626835775239, 0.269389736647874, 0.08310104843169575, 0.1855615584637631, 0.9712204833991392,
0.5661770294440235, 0.1131919985611245, 0.5970382608155964)
0.4998172726773653

 */
println(res)

/*
Davids-MacBook-Pro:Chapter03Arrays davidtan$ scala 05.scala
0.500520640859903

 */